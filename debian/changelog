php-horde-token (2.0.9-9) unstable; urgency=medium

  * d/watch: Switch to format version 4.
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.
  * d/t/control: Explicitly require php-horde-db (>= 2.4.1-8~) to silence PHP
    8.2 deprecation warnings in autopkgtest. (Closes: #1028949).
  * debian/patches: Add 1012_php8.2.patch. Fix PHP 8.2 deprecation warnings.
    (Closes: #1028949).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 Jan 2023 22:29:39 +0100

php-horde-token (2.0.9-8) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).
  * d/t/control: Require php-sqlite3.
  * d/t/control: Require php-horde-db.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 26 Jul 2020 23:08:50 +0200

php-horde-token (2.0.9-7) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 12:31:45 +0200

php-horde-token (2.0.9-6) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959284).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:52:00 +0200

php-horde-token (2.0.9-5) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:30:36 +0200

php-horde-token (2.0.9-4) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 09:31:41 +0200

php-horde-token (2.0.9-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 14:31:12 +0200

php-horde-token (2.0.9-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 07:04:11 +0200

php-horde-token (2.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 11:28:19 +0100

php-horde-token (2.0.8-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 11:50:47 +0100

php-horde-token (2.0.8-1) unstable; urgency=medium

  * New upstream version 2.0.8

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 22:23:54 +0100

php-horde-token (2.0.7-1) unstable; urgency=medium

  * Upgaded to debhelper compat 9
  * New upstream version 2.0.7

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 21:40:21 +0200

php-horde-token (2.0.6-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 00:04:14 +0200

php-horde-token (2.0.6-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.6

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 09:46:35 +0200

php-horde-token (2.0.5-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 13:32:53 +0200

php-horde-token (2.0.5-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 12:44:48 +0200

php-horde-token (2.0.5-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:40:59 +0200

php-horde-token (2.0.5-1) unstable; urgency=medium

  * New upstream version 2.0.5

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Jun 2014 20:40:20 +0200

php-horde-token (2.0.4-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 13:23:06 +0200

php-horde-token (2.0.4-1) unstable; urgency=low

  * New upstream version 2.0.4

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 20:59:21 +0200

php-horde-token (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 18:08:43 +0200

php-horde-token (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 22:40:32 +0100

php-horde-token (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 21:26:28 +0100

php-horde-token (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 22:07:13 +0100

php-horde-token (1.1.4-1) unstable; urgency=low

  * Horde_Token package.
  * Initial packaging (Closes: #635818)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 10 Jan 2012 22:36:27 +0100
